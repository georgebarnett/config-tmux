#!/bin/bash

REPOSITORY="https://gitlab.com/georgebarnett/config-tmux.git"
TMUX_CONFIG_ROOT="${HOME}/.config/tmux"

CONFIG_NAME="tmux.conf"
CONFIG_LOCAL_NAME="tmux.conf.local"

TMUX_CONFIG="${HOME}/.${CONFIG_NAME}"
TMUX_CONFIG_LOCAL="${HOME}/.${CONFIG_LOCAL_NAME}"

# ANSI escapes
RESTORE=$(echo -en '\033[0m')
RED=$(echo -en '\033[00;31m')
GREEN=$(echo -en '\033[00;32m')
BLUE=$(echo -en '\033[00;34m')

# Prints the arguments in green
success() {
  echo "${GREEN}${@}${RESTORE}"
}

# Prints the arguments in blue
info() {
  echo "${BLUE}${@}${RESTORE}"
}

# Prints the arguments in red
error() {
  echo "${RED}${@}${RESTORE}"
}

welcome_message() {
  info "Setting up tmux config 👌"
}

clone_tmux_config() {
  info "Cloning tmux configuration into ${TMUX_CONFIG_ROOT}"

  if [ -d "${TMUX_CONFIG_ROOT}" ]; then
    warn "${TMUX_CONFIG_ROOT} already exists, not cloning config"
  else
    git clone ${REPOSITORY} ${TMUX_CONFIG_ROOT}
    if [ $? -eq 0 ]; then
      success "Config cloned to ${TMUX_CONFIG_ROOT}"
    else
      error "Could not clone ${REPOSITORY}, see output for info"
      exit 1
    fi
  fi
}

link_config() {
  local config=$1
  local destination=$2
  info "Linking ${config} to ${destination}"
  ln -sv "${config}" "${destination}"
  if [ $? -eq 0 ]; then
    success "Linked ${config} to ${destination}"
  else
    error "Could not link ${config} to ${destination}"
    exit 1
  fi
}

# Do the setup!
welcome_message
clone_tmux_config
link_config "${TMUX_CONFIG_ROOT}/${CONFIG_NAME}" ${TMUX_CONFIG}
link_config "${TMUX_CONFIG_ROOT}/${CONFIG_LOCAL_NAME}" ${TMUX_CONFIG_LOCAL}
